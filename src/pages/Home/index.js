import React, {useState} from 'react';

import { View, Text, TextInput, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { AsyncStorage } from 'react-native';
import Login from '../Login';
import AppList from '../AppList';
import AppForm from '../AppForm';
import Home from './usuario';

const Drawer = createDrawerNavigator();

function MyDrawer() {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="Perfil" component={AppList} />
      <Drawer.Screen name="Alterar Dados" component={AppForm} />
      <Drawer.Screen name="Logoff" component={Login} options={{ headerShown: null }} />
    </Drawer.Navigator>
  );
}

export default function App() {
  return (
    <MyDrawer />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#2c3e50'
  },
  imgHome: {
    width: 330,
    height: 100,
    borderRadius: 2
  },
  imgProfile: {
    width: 330,
    height: 150,
    marginTop: 15,
    borderRadius: 2
  },
  botao: {
    width: 300,
    height: 45,
    backgroundColor: '#FFC125',
    marginTop: 20,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  botaoText: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  h3: {
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 10,
    fontSize: 22,
    color: '#fff'
  },
  h4: {
    alignItems: 'flex-start',
    margin: 3,
    marginTop: 10,
    marginBottom: 10,
    fontSize: 16,
    color: '#fff'
  },
  input: {
    marginTop: 10,
    padding: 10,
    width: 340,
    backgroundColor: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    borderRadius: 5
  },
});