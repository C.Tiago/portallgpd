import React, { Component } from 'react';
import { View, Text, TextInput, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { AsyncStorage } from 'react-native';


export default class Home extends Component{
	
	state = {
    usuario: "",
    cpf: "",
    mail: ""
	}

  handleSignInPress = async () => {
    this.props.navigation.navigate("Alterar Dados");
  }

	getNome = async() => {
    var nome_usuario = "";
    var aux = "";
		try{
			const username = await AsyncStorage.getItem('name')
			if(username !== null){
        aux = username.split(",");
        nome_usuario = aux[0];
			}
		}catch(e){
			console.log(e)
		}
		return nome_usuario;
  }
  
  getCPF = async() => {
    var cpf_usuario = "";
    var aux = "";
		try{
			const username = await AsyncStorage.getItem('name')
			if(username !== null){
        aux = username.split(",");
        cpf_usuario = aux[1];
			}
		}catch(e){
			console.log(e)
		}
		return cpf_usuario;
  }
  
  getMail = async() => {
    var mail_usuario = "";
    var aux = "";
		try{
			const username = await AsyncStorage.getItem('name')
			if(username !== null){
        aux = username.split(",");
        mail_usuario = aux[2];
			}
		}catch(e){
			console.log(e)
		}
		return mail_usuario;
	}
	
	async componentDidMount() {
		const nome_usuario = await this.getNome();
    this.setState({ usuario: nome_usuario});
    const cpf_usuario = await this.getCPF();
    this.setState({ cpf: cpf_usuario});
    const mail_usuario = await this.getMail();
    this.setState({ mail: mail_usuario});
    }

	render(){
		return(

            <View style={styles.container}>
                <Image 
                    source={require('../../img/20anos-gv-sitegv.jpg')}
                    style={styles.imgHome}
                />
                <Text
                    style={styles.h3}> 
                    Central do Cliente - Icarus 
                </Text>
                <Text style={styles.h4}> 
                    {this.state.usuario} , seja bem-vindo!
                </Text>
                <Text style={styles.h4}>
                    Confira aqui seus dados pessoais. 
                    Em caso de divengências basta acessar 
                    o menu lateral "Alterar Dados" e informe
                    seus dados corretos.
                </Text>
 
                <View style={styles.containerDados}>
                <Text style={styles.dados}> 
                    Nome: {this.state.usuario}
                </Text>
                <Text style={styles.dados}> 
                    CPF: {this.state.cpf}
                </Text>
                <Text style={styles.dados}> 
                    E-mail: {this.state.mail}
                </Text>
                
                  <TouchableOpacity
                    style={styles.botao}
                    onPress={this.handleSignInPress}
                  >
                    <Text style={styles.botaoText}>Alterar dados</Text> 
                  </TouchableOpacity>
                  </View>  
            </View>
		)
	}
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#2c3e50'
    },
    containerDados: {
      borderWidth: 3,
      borderRadius: 15,
      padding: 10,
      flex: 1,
      alignItems: 'center',
      backgroundColor: '#2c3e50'
    },
    imgHome: {
      marginTop: 1,
      width: 360,
      height: 100,
      borderRadius: 2
    },
    botao: {
      width: 300,
      height: 55,
      backgroundColor: '#FFC125',
      marginTop: 65,
      borderRadius: 5,
      alignItems: 'center',
      justifyContent: 'center'
    },
    botaoText: {
      fontSize: 16,
      fontWeight: 'bold'
    },
    h3: {
      alignItems: 'center',
      marginBottom: 10,
      marginTop: 10,
      fontSize: 22,
      color: '#fff'
    },
    h4: {
      alignItems: 'center',
      margin: 7,
      marginTop: 0,
      marginBottom: 20,
      fontSize: 16,
      color: '#fff'
    },
    dados: {
      alignItems: 'center',
      marginBottom: 10,
      marginTop: 20,
      fontSize: 22,
      color: '#fff'
    },
  });