import React from 'react';
import {StyleSheet, Text, Alert, View, TouchableOpacity} from 'react-native';
import { Feather as Icon } from '@expo/vector-icons';
import Database from '../../Database';
 
export default function AppItem(props){

    async function handleEditPress(){ 
        const item = await Database.getItem(props.id);
        console.log(item);
        props.navigation.navigate("Alterar Dados", item);
    }

    function handleDeletePress(){ 
        Alert.alert(
            "UAI!",
            "Quer excluir mesmo ou apertou sem querer?",
            [
                {
                text: "Sem querer",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
                },
                { text: "Excluir", onPress: () => {
                        Database.deleteItem(props.id)
                        .then(response => props.navigation.navigate("Perfil", {id: props.id}));
                    }
                }
            ],
            { cancelable: false }
        );
    }

    function handleEmailSend(){ 
        Alert.alert(
            "Atenção",
            "Um e-mail com os seus dados foi enviado para conferência e adequação de divergência!",
            [
                {
                text: "OK",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
                },
            ],
            { cancelable: false }
        );
    }

    return (
        <View style={styles.container}>
          <Text style={styles.textItem}>{props.item}</Text>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity 
                style={styles.deleteButton}
                onPress={handleDeletePress}>
                <Icon name="trash" color="grey" size={18} /> 
            </TouchableOpacity> 
            <TouchableOpacity 
                style={styles.editButton} 
                onPress={handleEditPress}> 
                <Icon name="edit" color="black" size={18} /> 
            </TouchableOpacity>
            <TouchableOpacity
                    style={styles.botao}
                    onPress={handleEmailSend}>
                    <Text style={styles.botaoText}>Informar Divergência</Text> 
                  </TouchableOpacity> 
          </View>
        </View>
      );
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#fff',
      marginTop: 20,
      width: '100%'
    },
    botao: {
        width: 170,
        height: 41,
        backgroundColor: '#FFC125',
        marginTop: 35,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
      },
      botaoText: {
        fontSize: 16,
        fontWeight: 'bold'
      },
    buttonsContainer: {
        flexDirection: 'row-reverse',
        alignItems: 'flex-end',
        borderBottomWidth: 1,
        borderBottomColor: '#CCC',
        paddingBottom: 10,
        marginTop: 10,
    },
    editButton: {
        marginLeft: 10,
        height: 40,
        backgroundColor: '#FFC125',
        borderRadius: 10,
        padding: 10,
        fontSize: 12,
        elevation: 10,
        shadowOpacity: 10,
        shadowColor: '#ccc',
        alignItems: 'center'
    },
    deleteButton: {
        marginLeft: 10,
        height: 40,
        width: 40,
        backgroundColor: '#8B0000',
        borderRadius: 10,
        padding: 10,
        fontSize: 12,
        elevation: 10,
        shadowOpacity: 10,
        shadowColor: '#ccc',
        alignItems: 'center'
    },
    buttonText: {
        color: '#000',
        fontWeight: 'bold',
    },
    textItem: {
        fontSize: 20,
    }
  });