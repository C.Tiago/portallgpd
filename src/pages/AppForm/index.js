import React, {useState, useEffect} from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import Database from '../../Database';

export default function AppForm({ route, navigation }) {

  const id = route.params ? route.params.id : undefined;
  const [nome, setNome] = useState(''); 
  const [cpf, setCPF] = useState('');
  const [mail, setMail] = useState('');

  useEffect(() => {
    if(!route.params) return;
    setNome(route.params.nome);
    setCPF(route.params.cpf);
    setMail(route.params.mail);
    }, [route])
  
  function handleDescriptionNome(nome){ setNome(nome); } 
  function handleDescriptionCPF(cpf){ setCPF(cpf); }
  function handleDescriptionMail(mail){ setMail(mail); }

  async function handleButtonPress(){
    console.log({id: 1, nome, cpf, mail});
    const listItem = {nome, cpf, mail};
    Database.saveItem(listItem, id)
    .then(response => navigation.navigate("Perfil", listItem));
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Editar Dados</Text>
      <View style={styles.inputContainer}> 
      
      <TextInput
      onChangeText={handleDescriptionNome} 
      style={styles.input}
      placeholder="Nome"
      clearButtonMode="always"
      value={nome}
    />
    <TextInput
      onChangeText={handleDescriptionCPF}
      style={styles.input}
      placeholder="CPF"
      clearButtonMode="always"
      value={cpf}
    />
    <TextInput
    onChangeText={handleDescriptionMail}
      style={styles.input}
      placeholder="e-mail"
      clearButtonMode="always"
      value={mail}
    />
    <TouchableOpacity
      style={styles.botao}
      onPress={handleButtonPress}
    >
    <Text style={styles.botaoText}>Salvar</Text> 
    </TouchableOpacity> 
      </View>
      <StatusBar style="light" />
    </View>
  );}
   
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#2c3e50',
      alignItems: 'center',
    },
    title: {
      color: '#fff',
      fontSize: 20,
      fontWeight: 'bold',
      marginTop: 50,
    },
    inputContainer: {
      flex: 1,
      marginTop: 30,
      width: '90%',
      padding: 20,
      borderTopLeftRadius: 10,
      borderTopRightRadius: 10,
      alignItems: 'stretch',
      backgroundColor: '#fff'
    },
    input: {
        marginTop: 10,
        padding: 10,
        width: 240,
        backgroundColor: '#fff',
        fontSize: 16,
        fontWeight: 'bold',
        borderRadius: 5
      },
    h4: {
        alignItems: 'flex-start',
        margin: 3,
        marginTop: 10,
        marginBottom: 10,
        fontSize: 16,
        color: '#fff'
      },
    botao: {
        width: 240,
        height: 45,
        backgroundColor: '#FFC125',
        marginTop: 50,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
      },
      botaoText: {
        fontSize: 16,
        fontWeight: 'bold'
      },
  });
  



