import React, { Component } from 'react';

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import Login from './src/pages/Login'
import Inicio from './src/pages/Home'

const Stack = createStackNavigator();

export default function App() {

  return(
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen 
        name="Home" 
        component={Inicio}
        options={{ headerShown: null }}
        />

      </Stack.Navigator>
    </NavigationContainer>
  )
}