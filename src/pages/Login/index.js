import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { AsyncStorage } from 'react-native';
import { StackActions } from '@react-navigation/stack'
import { Alert, StyleSheet, Text, View, Image, TextInput, TouchableOpacity} from 'react-native';
import { NavigationContainer, NavigationContext } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import api from '../../services/api';


export default class Login extends Component {

  static navigationOptions = {
    header: null,
  };

  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
      dispatch: PropTypes.func,
    }).isRequired,
  };

  state = {
    projeto: 'vocesaude',
    token_app: '',
    access_token: '',
    error: '',
  };
  
  handleToken_appChange = (token_app) => {
    this.setState({ token_app });
  };
  
  handleAccess_tokenChange = (access_token) => {
    this.setState({ access_token });
  };


  handleSignInPress = async () => {
    if (this.state.access_token.length === 0 || this.state.token_app.length === 0) {
      this.setState({ error: 'Preencha os dados corretamente para continuar!' }, () => false);
    } else {
      try {
        const response = await api.post('login?projeto='+
        this.state.projeto+
        '&token_app='+this.state.token_app+
        '&access_token='+this.state.access_token);

        await AsyncStorage.setItem('name', response.data.usuario.login_usuario);

        this.props.navigation.navigate('Home')

      } catch (_err) {
        console.log(_err)
        this.setState({ error: 'Houve um problema com o login, verifique suas credenciais!' });
      }
    }
  };

  render() {
  return(
  <View style={styles.container}>
    <Image 
    source={require('../../img/banner_grupovoce.png')}
    style={styles.logo}
    />

    <Text
      style={styles.h3}
    >Central do Cliente - Icarus</Text>

    <TextInput
      style={styles.input}
      //secureTextEntry={true}
      placeholder="Digite seu Login"
      value={this.state.token_app}
      onChangeText={this.handleToken_appChange}
      autoCapitalize="none"
    />

    <TextInput
      style={styles.input}
      placeholder="Digite sua senha"
      value={this.state.access_token}
      onChangeText={this.handleAccess_tokenChange}
      autoCapitalize="none"
    />
    
    <TouchableOpacity
      style={styles.botao}
      onPress={this.handleSignInPress}
    >
      <Text style={styles.botaoText}>Login</Text> 
    </TouchableOpacity>

    {this.state.error.length !== 0 && 
    <Text
    style={styles.error}
    >{this.state.error}</Text>}
  </View>
  )
}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c3e50'
  },
  logo: {
    width: 300,
    height: 300,
    borderRadius: 100
  },
  input: {
    marginTop: 10,
    padding: 10,
    width: 300,
    backgroundColor: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    borderRadius: 5
  },
  botao: {
    width: 300,
    height: 45,
    backgroundColor: '#FFC125',
    marginTop: 10,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center'
  },
  botaoText: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  h3: {
    fontSize: 22,
    color: '#fff'
  },
  error: {
    fontSize: 16,
    color: '#fff'
  }
});
